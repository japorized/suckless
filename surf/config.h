/* modifier 0 means no modifier */
static int surfuseragent    = 1;  /* Append Surf version to default WebKit user agent */
static char *fulluseragent  = ""; /* Or override the whole user agent string */
static char *scriptfile     = "~/.surf/script.js";
static char *styledir       = "~/.surf/styles/";
static char *certdir        = "~/.surf/certificates/";
static char *cachedir       = "~/.surf/cache/";
static char *cookiefile     = "~/.surf/cookies.txt";

static SearchEngine searchengines[] = {
    { "d",   "http://duckduckgo.com/?q=%s"   },
    { "leo", "http://dict.leo.org/ende?search=%s" },
};

/* Webkit default features */
/* Highest priority value will be used.
 * Default parameters are priority 0
 * Per-uri parameters are priority 1
 * Command parameters are priority 2
 */
static Parameter defconfig[ParameterLast] = {
	/* parameter                    Arg value       priority */
	[AcceleratedCanvas]   =       { { .i = 1 },     },
	[AccessMicrophone]    =       { { .i = 0 },     },
	[AccessWebcam]        =       { { .i = 0 },     },
	[Certificate]         =       { { .i = 0 },     },
	[CaretBrowsing]       =       { { .i = 0 },     },
	[CookiePolicies]      =       { { .v = "@Aa" }, },
	[DefaultCharset]      =       { { .v = "UTF-8" }, },
	[DiskCache]           =       { { .i = 1 },     },
	[DNSPrefetch]         =       { { .i = 0 },     },
	[FileURLsCrossAccess] =       { { .i = 0 },     },
	[FontSize]            =       { { .i = 12 },    },
	[FrameFlattening]     =       { { .i = 0 },     },
	[Geolocation]         =       { { .i = 0 },     },
	[HideBackground]      =       { { .i = 0 },     },
	[Inspector]           =       { { .i = 0 },     },
	[Java]                =       { { .i = 1 },     },
	[JavaScript]          =       { { .i = 1 },     },
	[KioskMode]           =       { { .i = 0 },     },
	[LoadImages]          =       { { .i = 1 },     },
	[MediaManualPlay]     =       { { .i = 1 },     },
	[Plugins]             =       { { .i = 1 },     },
	[PreferredLanguages]  =       { { .v = (char *[]){ NULL } }, },
	[RunInFullscreen]     =       { { .i = 0 },     },
	[ScrollBars]          =       { { .i = 1 },     },
	[ShowIndicators]      =       { { .i = 1 },     },
	[SiteQuirks]          =       { { .i = 1 },     },
	[SmoothScrolling]     =       { { .i = 0 },     },
	[SpellChecking]       =       { { .i = 0 },     },
	[SpellLanguages]      =       { { .v = ((char *[]){ "en_US", NULL }) }, },
	[StrictTLS]           =       { { .i = 1 },     },
	[Style]               =       { { .i = 1 },     },
	[WebGL]               =       { { .i = 0 },     },
	[ZoomLevel]           =       { { .f = 1.2 },   },
};

static UriParameters uriparams[] = {
	{ "(://|\\.)suckless\\.org(/|$)", {
	    [JavaScript] = { { .i = 0 }, 1 },
	    [Plugins]    = { { .i = 0 }, 1 },
	}, },
  { "(://|\\.)japorized\\.github\\.io(/|$)", {
      [ZoomLevel] = { { .f = 1.2 }, 1.0 },
  }, },
  { "(://|\\.)japorized\\.ink(/|$)", {
      [ZoomLevel] = { { .f = 1.2 }, 1.0 },
  }, },
  { "(://|\\.)messenger\\.com(/|$)", {
      [ZoomLevel] = { { .f = 1.2 }, 1.0 },
	    [RunInFullscreen] = { { .i = 1 },     },
  }, },
  { "(://|\\.)duckduckgo\\.com(/|$)", {
      [ZoomLevel] = { { .f = 2.0 }, 1.0 },
  }, },
};

/* default window size: width, height */
static int winsize[] = { 800, 600 };

static WebKitFindOptions findopts = WEBKIT_FIND_OPTIONS_CASE_INSENSITIVE |
                                    WEBKIT_FIND_OPTIONS_WRAP_AROUND;

#define PROMPT_GO   "Url"
#define PROMPT_FIND "Find"

/* SETPROP(readprop, setprop, prompt)*/
#define SETPROP(r, s, p) { \
        .v = (const char *[]){ "/bin/sh", "-c", \
             "prop=\"$(printf '%b' \"$(xprop -id $1 $2 " \
             "| sed \"s/^$2(STRING) = //;s/^\\\"\\(.*\\)\\\"$/\\1/\")\" " \
             "| dmenu -p \"$4\" -w $1)\" && xprop -id $1 -f $3 8s -set $3 \"$prop\"", \
             "surf-setprop", winid, r, s, p, NULL \
        } \
}

/* DOWNLOAD(URI, referer) */
/* #define DOWNLOAD(u, r) { \ */
/*         .v = (const char *[]){ "st", "-e", "/bin/sh", "-c",\ */
/*              "curl -g -L -J -O -A \"$1\" -b \"$2\" -c \"$2\"" \ */
/*              " -e \"$3\" \"$4\"; read", \ */
/*              "surf-download", useragent, cookiefile, r, u, NULL \ */
/*         } \ */
/* } */
#define DOWNLOAD(u, r) { \
  .v = (const char *[]){ "st", "-T", "surf-download", "-n", "surf-download", "-e", \
       "/bin/sh", "-c", "cd ${HOME}/Downloads && curl -A 'Mozilla/5.0' -c '${HOME}/.surf/cookies.txt' -4 -L -O \"$@\" && exit", \
       "surf-download", u, r, NULL \
  } \
}

/* PLUMB(URI) */
/* This called when some URI which does not begin with "about:",
 * "http://" or "https://" should be opened.
 */
#define PLUMB(u) {\
        .v = (const char *[]){ "/bin/sh", "-c", \
             "xdg-open \"$0\"", u, NULL \
        } \
}

/* VIDEOPLAY(URI) */
#define VIDEOPLAY(u) {\
        .v = (const char *[]){ "/bin/sh", "-c", \
             "mpv --really-quiet \"$0\"", u, NULL \
        } \
}

/* styles */
/*
 * The iteration will stop at the first match, beginning at the beginning of
 * the list.
 */
static SiteSpecific styles[] = {
	/* regexp               file in $styledir */
	{ ".*",                 "default.css" },
};

/* certificates */
/*
 * Provide custom certificate for urls
 */
static SiteSpecific certs[] = {
	/* regexp               file in $certdir */
	{ "://suckless\\.org/", "suckless.org.crt" },
};

#define MODKEY GDK_CONTROL_MASK
#define SHIFT GDK_SHIFT_MASK
#define ALTKEY GDK_MOD1_MASK

/* hotkeys */
/*
 * If you use anything else but MODKEY and SHIFT, don't forget to
 * edit the CLEANMASK() macro.
 */
static Key keys[] = {
	/* modifier              keyval          function    arg */
	{ MODKEY,                GDK_KEY_g,      spawn,      SETPROP("_SURF_URI", "_SURF_GO", PROMPT_GO) },
	{ MODKEY,                GDK_KEY_slash,  spawn,      SETPROP("_SURF_FIND", "_SURF_FIND", PROMPT_FIND) },
	{ 0,                     GDK_KEY_Escape, stop,       { 0 } },
	{ MODKEY|SHIFT,          GDK_KEY_r,      reload,     { .i = 1 } } ,
	{ MODKEY,                GDK_KEY_r,      reload,     { .i = 0 } } ,

	/* { MODKEY,                GDK_KEY_l,      navigate,   { .i = +1 } }, */
	/* { MODKEY,                GDK_KEY_h,      navigate,   { .i = -1 } }, */
	{ MODKEY,        GDK_KEY_Right,      navigate,   { .i = +1 } },
	{ MODKEY,        GDK_KEY_Left,       navigate,   { .i = -1 } },

	/* Currently we have to use scrolling steps that WebKit2GTK+ gives us
	 * d: step down, u: step up, r: step right, l:step left
	 * D: page down, U: page up */
	{ MODKEY,                GDK_KEY_j,      scroll,     { .i = 'd' } },
	{ MODKEY,                GDK_KEY_k,      scroll,     { .i = 'u' } },
	{ MODKEY,                GDK_KEY_b,      scroll,     { .i = 'U' } },
	{ MODKEY,                GDK_KEY_f,      scroll,     { .i = 'D' } },
	{ MODKEY,                GDK_KEY_l,      scroll,     { .i = 'r' } },
	{ MODKEY,                GDK_KEY_h,      scroll,     { .i = 'l' } },

	{ MODKEY       , GDK_KEY_equal , zoom , { .i = 0  } } ,
	{ MODKEY       , GDK_KEY_minus , zoom , { .i = -1 } } ,
	{ MODKEY|SHIFT , GDK_KEY_plus  , zoom , { .i = +1 } } ,

	{ MODKEY|SHIFT , GDK_KEY_c , clipboard , { .i = 1 } } ,
	{ MODKEY|SHIFT , GDK_KEY_v , clipboard , { .i = 0 } } ,

	{ MODKEY       , GDK_KEY_n,      find,       { .i = +1 } },
	{ MODKEY|SHIFT , GDK_KEY_n,      find,       { .i = -1 } },

	{ MODKEY|SHIFT , GDK_KEY_p,      print    , { 0 } } ,
	{ MODKEY       , GDK_KEY_t,      showcert , { 0 } } ,

	{ MODKEY|SHIFT , GDK_KEY_a   , togglecookiepolicy , { 0 } } ,
	{ 0            , GDK_KEY_F11 , togglefullscreen   , { 0 } } ,
	{ MODKEY|SHIFT , GDK_KEY_o   , toggleinspector    , { 0 } } ,

	/* { MODKEY|SHIFT, GDK_KEY_c,      toggle,     { .i = CaretBrowsing } }, */
	/* { MODKEY|SHIFT, GDK_KEY_f,      toggle,     { .i = FrameFlattening } }, */
	/* { MODKEY|SHIFT, GDK_KEY_g,      toggle,     { .i = Geolocation } }, */
	/* { MODKEY|SHIFT, GDK_KEY_s,      toggle,     { .i = JavaScript } }, */
	/* { MODKEY|SHIFT, GDK_KEY_i,      toggle,     { .i = LoadImages } }, */
	/* { MODKEY|SHIFT, GDK_KEY_v,      toggle,     { .i = Plugins } }, */
	/* { MODKEY|SHIFT, GDK_KEY_b,      toggle,     { .i = ScrollBars } }, */
	/* { MODKEY|SHIFT, GDK_KEY_t,      toggle,     { .i = StrictTLS } }, */
	/* { MODKEY|SHIFT, GDK_KEY_m,      toggle,     { .i = Style } }, */
};

/* button definitions */
/* target can be OnDoc, OnLink, OnImg, OnMedia, OnEdit, OnBar, OnSel, OnAny */
static Button buttons[] = {
	/* target       event mask      button  function        argument        stop event */
	{ OnLink,       0,              2,      clicknewwindow, { .i = 0 },     1 },
	{ OnLink,       MODKEY,         2,      clicknewwindow, { .i = 1 },     1 },
	{ OnLink,       MODKEY,         1,      clicknewwindow, { .i = 1 },     1 },
	{ OnAny,        0,              8,      clicknavigate,  { .i = -1 },    1 },
	{ OnAny,        0,              9,      clicknavigate,  { .i = +1 },    1 },
	{ OnMedia,      MODKEY,         1,      clickexternplayer, { 0 },       1 },
};

#define HOMEPAGE "https://japorized.github.io/elementary"
