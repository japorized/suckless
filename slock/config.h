/* user and group to drop privileges to */
static const char *user  = "japorized";
static const char *group = "users";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#6e69a8",   /* during input */
	[FAILED] = "#1c1c1c",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
