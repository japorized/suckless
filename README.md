# My suckless storage

Truth is, I can barely program in C or C++ given that I have never learnt it, nor do I have the time do learn it for now. These are, however, tools that I would like to explore and use from [suckless.org](https://suckless.org).

Basically, I still suck, but I need a place to store stuff that is not on my local machine.
